#include "originalPC.h" // including OriginalPC header file so I can create implementations for some of its functions

// assigning values and creating objects within the OriginalPC constructor
OriginalPC::OriginalPC(): currentHealth(10), maxHealth(10), strength(10), speed(10), currentMoney(0), visible(true), numberOfEnemiesKilled(0)
{
	// passing in the Color parameters and Creating New Rect Object
	marker = new Rect(0, 0, 0.15, 0.15, 1.0f, 1.0f, 1.0f);

	//set the color, text and size of the label
	SDL_Color colour = { 255, 255, 0 };
	name = new Label("Player",TTF_OpenFont("MavenPro-Regular.ttf", 24),colour);
}

// used to update/change the position of the Rect object called marker by passing in parameters for x and y
void OriginalPC::updatePosition(float xPos, float yPos)
{
	//setting the marker position
	marker->setX(xPos);
	marker->setY(yPos);
}

//used to change the OriginalPCs base stats(strength, speed, health) by passing in values  
void OriginalPC::changeBaseStats(int str, int sp, int hp)
{
	strength += str;
	speed += sp;

	// checking if the players current health is the same as the players maximum health and then healing him if it isn't
	// and upgrading his maximum health and currenthealth if it is
	if(currentHealth == maxHealth)
	{
		maxHealth += hp;
		currentHealth += hp;
	}
	else if(hp > 0)
	{
		currentHealth = maxHealth;
	}
}

// used to reset the OriginalPCs variables to there default state
void OriginalPC::resetVariables()
{
	strength = 10;
	speed = 10;
	maxHealth = 10;
	currentHealth = 10;
	numberOfEnemiesKilled = 0;
	currentMoney = 0;
	visible = true;
}

// used to draw the parts of OriginalPC to screen that are required(could for instance also draw the label to screen at the marker position)
void OriginalPC::draw()
{
	// calling the marker(Rect) draw function
	marker->draw();
	
	// draw label and position it on the marker
	drawLabel(marker->getX()+(marker->getW()/2.0f), marker->getY()+marker->getH());
}

// for drawing the OriginalPC's name(label) to screen pass in parameters for where you wish it to be drawn
void OriginalPC::drawLabel(float x, float y)
{
	// calling the name(Label) draw function
	name->draw(x, y);
}
 
 // function to set the stats, difference from changeStat function is that this one is for setting entirely new stats by using the =, rather than editing pre-existing stats as well as setting currentMoney and enemies killed
void OriginalPC::setStats(int str, int sp, int chp, int mhp, int cm, int ek)
{
	strength = str;
	speed = sp;
	maxHealth = mhp;
	currentHealth = chp;
	currentMoney = cm;
	numberOfEnemiesKilled = ek;
}

// OriginalPC deconstructor delete objects and do other things that are required for the Deletion of OriginalPC
OriginalPC::~OriginalPC()
{
	// delete the objects instantiated
	delete marker;
	delete name;
}