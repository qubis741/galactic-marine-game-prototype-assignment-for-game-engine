#include "stateMain.h" // including the stateMain header so I can code implementations of the functions
#include "game.h"      // including game so I can call various Game functions (like getSDLWindow, or state related functions)

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateMain::StateMain()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateMain::init()
{
	// creating new labels
	SDL_Color colour = { 255, 255, 0 };
	mainMenuLabel = new Label("Main Menu", TTF_OpenFont("MavenPro-Regular.ttf", 46), colour);
	newGameLabel = new Label("New Game (N)", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
	continueGameLabel = new Label("Continue Game (C)", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
	quitLabel = new Label("Quit (Q / ESC)", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
	creditsLabel = new Label("Credits (R)", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateMain::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	// drawing labels to screen
	mainMenuLabel->draw(-0.15, 0.9);
	newGameLabel->draw(-0.1, 0.7);
	continueGameLabel->draw(-0.1, 0.5);
	quitLabel->draw(-0.1, 0.3);
	creditsLabel->draw(-0.1, 0.1);

	
	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateMain::update()
{
	draw();
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateMain::handleSDLEvent(SDL_Event const &sdlEvent)
{
  if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{

			// sets the current state to Play state when n/N is pressed also changes the new game bool to true so the code knows its a new game
			case 'n': case 'N': 
				Game::getInstance()->setState(Game::getInstance()->getCharacterSelectState());
				Game::getInstance()->changeNewGame(true);
			break;

			// sets the current state to Play state when c/C is pressed checks if continueGame is true(gets set if there is a game already in play) then sets the bool to false
			case 'c': case 'C':
				if(Game::getInstance()->getContinueGameBool())
				{
					Game::getInstance()->setState(Game::getInstance()->getPlayState());
				}
			break;

			// checks if a game is in play if it is it switches to the exitPromptState if not then an SDL quit even is started 
			case 'q': case 'Q':
			case SDLK_ESCAPE:
				if(Game::getInstance()->getContinueGameBool())
				{
					Game::getInstance()->setState(Game::getInstance()->getExitPromptState());
				}
				else
				{
					SDL_Event quitEvent;
					quitEvent.type = SDL_QUIT;
					SDL_PushEvent(&quitEvent);
				}
			break;

			//sets the current State to the Credit State when R/r is pressed
			case 'r': case 'R':
				Game::getInstance()->setState(Game::getInstance()->getCreditsState());
			break;

			//if any other key is pressed take no action
			default:
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateMain::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateMain::enter()
{
}

// stateMain destructor deleting various objects that get instantiated within this class
StateMain::~StateMain()
{
	// deleting any objects I instantiated within this class
	delete mainMenuLabel;
	delete creditsLabel;
	delete newGameLabel;
	delete continueGameLabel;
	delete quitLabel;
}