#include "rect.h"     // including rect header so i can implement functions for the rect class and the GL/glew header so that I can use the types and functions of OpenGL
#include <GL/glew.h>

// constructor that sets up defualt positions, heights, widths and takes in the rect color as parameters
Rect::Rect(float red, float green, float blue) : xPos(0.0f), yPos(0.0f), width(0.1f), height(0.1f), R(red), G(green), B(blue) 
{
}

// constructor that allows you to pass in the position, colour, height and width of the rect as parameters
Rect::Rect(float x, float y, float w, float h, float red, float green, float blue) : xPos(x), yPos(y), width(w), height(h), R(red), G(green), B(blue)  
{
}

// draw function that uses the colours, positions and size to create the rectangle
void Rect::draw(void) {
	glColor3f(R,G,B);
	glBegin(GL_POLYGON); 
	  glVertex3f (xPos, yPos, 0.0); // first corner
	  glVertex3f (xPos+width, yPos, 0.0); // second corner
	  glVertex3f (xPos+width, yPos+height, 0.0); // third corner
	  glVertex3f (xPos, yPos+height, 0.0); // fourth corner
	glEnd();
}

// destructor that's blank
Rect::~Rect(void)
{
}
