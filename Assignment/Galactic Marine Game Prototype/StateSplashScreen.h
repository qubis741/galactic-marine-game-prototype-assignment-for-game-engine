#ifndef STATESPLASHSCREEN_H
#define STATESPLASHSCREEN_H
#include "gameState.h" // including gameState so StateSplashScreen can inherit from gameState and label so I can create label objects 
#include "label.h"

// as you can see below StateSplashScreen is a culmination of all of the inherited Abstract classes functions
class StateSplashScreen: public GameState{

public:
	StateSplashScreen();                               // StateSplashScreen constructor
	void draw();                                       // Function that is used to draw collections of other objects during this state
    void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void update();                                     // Function that is used to update this state 
    void handleSDLEvent(SDL_Event const &sdlEvent);    // Function that is used to handleSDLEvents, all keyboard events at the moment
    void exit();                                       // Function that should be called when the state is switching, anything you wish to happen when you exit this state should go in this function
    void enter();                                      // Function that should be called when the state is switching, anything you wish to happen when you enter this state should go in this function
	~StateSplashScreen();                              // StateSplashScreen destructor
	
private:
	// other variables and/or object pointers that StatePlay requires the identifiers are descriptive
	// I shall explain the use for some of the ones thats usage may seem slightly unusual
	Label * developerLogoLabel;

	float entryTime; // variable to hold the initial time of entry to the state(for the in state timer)
};

#endif