#ifndef STATEMAIN_H
#define STATEMAIN_H
#include "gameState.h" // including gameState so StateMain can inherit from gameState and label so I can create label objects 
#include "label.h"

// as you can see below StateMain is a culmination of all of the inherited Abstract classes functions
class StateMain: public GameState{

public:
	StateMain();                                       // StateMain constructor
    void draw();                                       // Function that is used to draw collections of other objects during this state
    void init();                                       // Function that is used to initilize all of the things that are required to run this state(instead of using the constructor)
    void update();                                     // Function that is used to update this state 
    void handleSDLEvent(SDL_Event const &sdlEvent);    // Function that is used to handleSDLEvents, all keyboard events at the moment
    void exit();                                       // Function that should be called when the state is switching, anything you wish to happen when you exit this state should go in this function
    void enter();                                      // Function that should be called when the state is switching, anything you wish to happen when you enter this state should go in this function
	~StateMain();                                      // StateMain destructor
	
private:
	// variables and/or object pointers that StateMain requires the identifiers are descriptive
	Label * mainMenuLabel;
	Label * creditsLabel;
	Label * newGameLabel;
	Label * continueGameLabel;
	Label * quitLabel;
};

#endif