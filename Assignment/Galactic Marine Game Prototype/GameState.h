#ifndef GAMESTATE_H
#define GAMESTATE_H
#include<SDL.h> // including SDL so that we can use SDL functions and types

// GameState needs to refer to the Game object which needs to
// refer to GameState objects, which creates a circular dependency
// and causes problems with #includes 
// solution is to declare (but not define) the Game type here.
// so instead of 
// #include "Game.h"
// we have a 'forward declaration' of the class:
class Game;
// Game.h will still need to be included in the state implementation files


// Abstract base class for GameState: no implementation as it is an abstract class and all of the games states shall inherit 
// from this and will require the following functions and objects 
class GameState{

public:
// all of the functions below are pure virtual functions so any class that inherits will need to implement its own or else be classed as abstract
    virtual void draw() = 0;                                       // function that draws objects to screen using the Game Window variable
    virtual void init() = 0;                                       // function that is used to initilize all of the States variables and objects
    virtual void update() = 0;                                     // function that is used to update the state (anything that happens while the stat is occuring should go in here)
    virtual void handleSDLEvent(SDL_Event const &sdlEvent) = 0;    // function used to handle SDL keyboard events
    virtual void exit() = 0;                                       // function that gets called when a state is getting changed anything that is required to happen when a state exits should go in here
    virtual void enter() = 0;                                      // function that gets called when a state is getting changed anything that is required to happen on entry of a state should go in here
    virtual ~GameState() {}                                        // blank destructor
};

#endif

