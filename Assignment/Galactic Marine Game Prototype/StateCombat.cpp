#include "stateCombat.h"   // including the stateCombat header so I can code implementations of the functions
#include "game.h"          // including game so I can call various Game functions (like getSDLWindow, or state related functions)
#include <sstream>         // including string stream so I can stream the enemy/player stats into the label class
#include <ctime>           // C time libraries for rand and time functions

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateCombat::StateCombat()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateCombat::init()
{
	// setting up random to use time to randomize
	std::srand( std::time(NULL) );

	// creating Labels
	SDL_Color colour = { 255, 255, 0 }; // creating an SDL Color 
	enemyStatsLabel = new Label();
	playerStatsLabel = new Label();
	playerWonLabel = new Label();
	enemyWonLabel = new Label();
	
	// set booleans to there initial settings
	combatHasBeenResolved = false; 
	enemyWon = false;
	playerWon = false;
}

// function to draw all of the various things that are required to be drawn during this state
void StateCombat::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
	
	std::stringstream playerStream;
	std::stringstream enemyStream;
	
	//draw enemy name label
	Game::getInstance()->getCurrentEnemy()->name->draw(0.150, -0.2);
	
	// input enemy stats into string stream
 	enemyStream << "  Health: " << Game::getInstance()->getCurrentEnemy()->getMaxHealth();
 	enemyStream << "  Strength: " << Game::getInstance()->getCurrentEnemy()->getStrength();
	enemyStream << "  Speed: " << Game::getInstance()->getCurrentEnemy()->getSpeed();

	// draw enemy stats to screen and set there position
	enemyStatsLabel->textToTexture(enemyStream.str().c_str(), TTF_OpenFont("MavenPro-Regular.ttf", 16));
    enemyStatsLabel->draw(0.3, -0.2);

	// input player stats into string stream
 	playerStream << "  Health: " << Game::getInstance()->getCurrentPlayer()->getCurrentHealth();
 	playerStream << "  Strength: " << Game::getInstance()->getCurrentPlayer()->getStrength();
	playerStream << "  Speed: " <<  Game::getInstance()->getCurrentPlayer()->getSpeed();
	playerStream << "  Current Money: " <<  Game::getInstance()->getCurrentPlayer()->getCurrentMoney();
	
	// draw player stats to screen and set there position
	playerStatsLabel->textToTexture(playerStream.str().c_str(), TTF_OpenFont("MavenPro-Regular.ttf", 16));
    playerStatsLabel->draw(-0.9, -0.2);
	
	// call draw for the current enemy and current player 
	Game::getInstance()->getCurrentEnemy()->draw();
	Game::getInstance()->getCurrentPlayer()->draw();

	// if playerWon is true then draw the playerWonLabel to screen
	if(playerWon == true)
	{
		playerWonLabel->textToTexture("You Have Vanquished The Monster", TTF_OpenFont("MavenPro-Regular.ttf", 24));
		playerWonLabel->draw(-0.5, 0.2);
	}

	// if enemyWon is true then draw the enemyWonLabel to screen
 	if(enemyWon == true)
	{
		enemyWonLabel->textToTexture("You Have Been Defeated", TTF_OpenFont("MavenPro-Regular.ttf", 24));
		enemyWonLabel->draw(-0.5, 0.2);
	}

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateCombat::update()
{
	draw();
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateCombat::handleSDLEvent(SDL_Event const &sdlEvent)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{

			// if return key or space key is pressed check if combatHasBeenResolved is false if it's false call the randomCombatResolution function else change state back to play state
			case SDLK_RETURN: case SDLK_SPACE:
				if(combatHasBeenResolved == false)
					randomCombatResolution(Game::getInstance()->getCurrentEnemy(), Game::getInstance()->getCurrentPlayer());
				else
					Game::getInstance()->setState(Game::getInstance()->getPlayState());
				break;

			// if any other key is pressed do nothing
			default:
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateCombat::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateCombat::enter()
{
	// need to reset these variables every time so that combat is properly reset each time
	combatHasBeenResolved = false; 
	playerWon = false;
	enemyWon = false;

	Game::getInstance()->getCurrentEnemy()->updatePosition(0.2, 0);   // update the players current position
	Game::getInstance()->getCurrentPlayer()->updatePosition(-0.8, 0); // update the enemys current position
}

// this is the version that implements random battles however I have left the normal
// combat mode in as well so that the game is easier to test (for win scenarios etc.)
void StateCombat::randomCombatResolution(AbstractNPC * enemy, AbstractPC * player)
{
	bool playerAttacksFirst;
	int damageDoneToPlayer = 0;
	int enemyAttackDamage = 0;
	int playerAttackDamage = 0;

	// if the players speed is greater than the enemies set playerAttacksFirst to true to
	// indicate the player is quicker than the enemy else set it to false
	if(player->getSpeed() > enemy->getSpeed())
	{
		playerAttacksFirst = true;  
	}
	else
		playerAttacksFirst = false;  
	
	// do while the enemy and players health are greater than 0(while one is currently alive)
	do
	{
		// if playerAttacksFirst is true then go into the attack seqence below where the player
 		// attacks first else go on to the next sequence
		if(playerAttacksFirst == true)
		{
			
			// if the player is "alive" then the player attacks the enemy by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(player->getCurrentHealth() > 0 && enemy->getMaxHealth() > 0)
			{
				 playerAttackDamage = rand() % (player->getStrength()-1) + 2;
				 enemy->changeBaseStats(0, 0, -playerAttackDamage);
			}
			// if the enemy is "alive" then the enemy attacks the player by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)
			{
				enemyAttackDamage = rand() % (enemy->getStrength()-1) + 2;
				damageDoneToPlayer += enemyAttackDamage;
				player->changeCurrentHealth(-enemyAttackDamage);
			}
			
		}
		else
		{
			// if the enemy is "alive" then the enemy attacks the player by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)
			{
				 playerAttackDamage = rand() % (player->getStrength()-1) + 2;
				 enemy->changeBaseStats(0, 0, -playerAttackDamage);
			}
			
			// if the player is "alive" then the player attacks the enemy by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)	
			{
				enemyAttackDamage = rand() % (enemy->getStrength()-1) + 2;
				damageDoneToPlayer += enemyAttackDamage;
				player->changeCurrentHealth(-enemyAttackDamage);
			}
		}
	}
	while(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0);
	



	if(enemy->getMaxHealth() <= 0) // if the enemys health is less than or equal to 0
	{
		// set the enemys visible boolean to false, now he won't show or be interactable
		// with in statePlay
		enemy->changeBoolVisible(false);

		// add the damage done to the player divided by two in this combat state on to
		// the player
		player->changeCurrentHealth(damageDoneToPlayer / 2);

		// increase the players money by the enemy money drop count
		player->changeCurrentMoney(enemy->getMoneyDrop());
		
		//increment the number of enemies killed inside the player class by 1
		player->changeNumberOfEnemiesKilled(1);

		//set the boolean to true so that the player win message now draws to screen 
		playerWon = true;

		// item drop chance is a random number between 1 and 100
		int itemDropChance = rand() % 100 + 1; 

		//if the itemDropChance is less than or equal to the current enemies drop rate
		//then call the itemDrop function(dictates which item type shall drop)
		if(itemDropChance <= enemy->getDropRate())
			itemDrop(player);
	}
	else if(player->getCurrentHealth() <= 0) // if the players health is less than or equal to 0
	{
		// change player visible boolean to false (effectivley killing the player)
		player->changeBoolVisible(false);
		
		//set the boolean to true so that the player defeat message now draws to screen
		enemyWon = true;
	}

	// set the combat has been resolved boolean to true so that the player can now press enter
	// to move back to the state play area (map state)
	combatHasBeenResolved = true;
}

// this is the normal combat resolution system where the character with the larger stats
// always wins
void StateCombat::combatResolution(AbstractNPC * enemy, AbstractPC * player)
{
	bool playerAttacksFirst;
	int damageDoneToPlayer = 0;
	int enemyAttackDamage = 0;

	// if the players speed is greater than the enemies set playerAttacksFirst to true to
	// indicate the player is quicker than the enemy else set it to false
	if(player->getSpeed() > enemy->getSpeed())
	{
		playerAttacksFirst = true;  
	}
	else
		playerAttacksFirst = false;  
	
	// do while the enemy and players health are greater than 0(while one is currently alive)
	do
	{
		// if playerAttacksFirst is true then go into the attack seqence below where the player
 		// attacks first else go on to the next sequence
		if(playerAttacksFirst == true)
		{
			// if the player is "alive" then the player attacks the enemy by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(player->getCurrentHealth() > 0 && enemy->getMaxHealth() > 0)
				 enemy->changeBaseStats(0, 0, -player->getStrength());
			
			// if the enemy is "alive" then the enemy attacks the player by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)
			{
				enemyAttackDamage = enemy->getStrength();
				damageDoneToPlayer += enemyAttackDamage;
				player->changeCurrentHealth(-enemyAttackDamage);
			}
			
		}
		else
		{
			// if the enemy is "alive" then the enemy attacks the player by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)
				enemy->changeBaseStats(0, 0, -player->getStrength());
			
			// if the player is "alive" then the player attacks the enemy by deducting a 
			//random number generated between the 2 and the amount of strength from its health
			if(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0)	
			{
				enemyAttackDamage = enemy->getStrength();
				damageDoneToPlayer += enemyAttackDamage;
				player->changeCurrentHealth(-enemyAttackDamage);
			}
		}
	}
	while(enemy->getMaxHealth() > 0 && player->getCurrentHealth() > 0);
	



	if(enemy->getMaxHealth() <= 0) // if the enemys health is less than or equal to 0
	{
		// set the enemys visible boolean to false, now he won't show or be interactable
		// with in statePlay
		enemy->changeBoolVisible(false);

		// add the damage done to the player divided by two in this combat state on to
		// the player
		player->changeCurrentHealth(damageDoneToPlayer / 2);

		// increase the players money by the enemy money drop count
		player->changeCurrentMoney(enemy->getMoneyDrop());
		
		//increment the number of enemies killed inside the player class by 1
		player->changeNumberOfEnemiesKilled(1);

		//set the boolean to true so that the player win message now draws to screen 
		playerWon = true;

		// item drop chance is a random number between 1 and 100
		int itemDropChance = rand() % 100 + 1; 

		//if the itemDropChance is less than or equal to the current enemies drop rate
		//then call the itemDrop function(dictates which item type shall drop)
		if(itemDropChance <= enemy->getDropRate())
			itemDrop(player);
	}
	else if(player->getCurrentHealth() <= 0) // if the players health is less than or equal to 0
	{
		// change player visible boolean to false (effectivley killing the player)
		player->changeBoolVisible(false);
		
		//set the boolean to true so that the player defeat message now draws to screen
		enemyWon = true;
	}

	// set the combat has been resolved boolean to true so that the player can now press enter
	// to move back to the state play area (map state)
	combatHasBeenResolved = true;
}

void StateCombat::itemDrop(AbstractPC * player)
{
	// get a random number between 1 and 100 
	int itemTypeChance = rand() % 100 + 1; 
	
	// if the number is less than or equal to 20 it's a stimulant(speed increase) else if it's
	// between 20 and 80 it's a health pack (increases max health by one or fully recovers current health)
	// if its between 80 to 100 then it's a combat pack (increase strength by 2)
	if(itemTypeChance <= 20) 
	{
		player->changeBaseStats(0, 1, 0); // stimulant
	}
	else if(itemTypeChance > 20 && itemTypeChance <= 80) 
	{
		player->changeBaseStats(0, 0, 1); // health pack
	}
	else if(itemTypeChance > 80 && itemTypeChance <= 100)
	{
		player->changeBaseStats(2, 0, 0); // combat pack
	}
}

// stateCombat destructor deleting various objects that get instantiated within this class
StateCombat::~StateCombat()
{
	// deleting the labels created within this class
	delete enemyWonLabel;
	delete playerWonLabel;
	delete enemyStatsLabel;
	delete playerStatsLabel;
}