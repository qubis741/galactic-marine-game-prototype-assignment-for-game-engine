#include "game.h"                     // including game header so that we can implement the functions, and including all of the various states so that I can instantiate them and create pointers to them
#include "stateMain.h"
#include "statePlay.h"
#include "stateCredits.h"
#include "stateSplashScreen.h"
#include "stateCharacterSelect.h"
#include "stateGameOver.h"
#include "stateGameWon.h"
#include "stateCombat.h"
#include "statePause.h"
#include "stateExitPrompt.h"
#include <fstream>                  // including file stream and string so that I can check if the save file is empty on start up
#include <string>                   

// setting the instance pointer to start at null
Game * Game::instance = NULL;

Game::Game()
{

   window = setupRC(glContext); // Create window and render context

   // checking if thre is a save file on start up
	std::ifstream readSaveFile;
	readSaveFile.open("saveFile.txt");

	std::string loadedVariable; 

	// if the file is open then take the first line and see if it's empty if it is then set continue to false as well as if 
	// there is a saved game to false and newGame to true, if it isn't set continue to true, save game to true and new game to false
	if (readSaveFile.is_open())
	{  
		// getting in the players state, position, stats, and enemieskilled from the file and then setting them
		getline(readSaveFile, loadedVariable);
		
		if(loadedVariable == "")
		{
			continueGame = false;
			isThereASavedGame = false;
			newGame = true;
		}
		else
		{
			continueGame = true;
			isThereASavedGame = true;
			newGame = false;
		}
	}

   // setting the booleans to there initial values
   running = true;
   
   // creating objects of all the states
   creditsState = new StateCredits();
   playState = new StatePlay();
   mainMenuState = new StateMain();
   splashScreenState = new StateSplashScreen();
   characterSelectState = new StateCharacterSelect();
   gameOverState = new StateGameOver();
   gameWonState = new StateGameWon();
   combatState = new StateCombat();
   pauseState = new StatePause();
   exitPromptState = new StateExitPrompt();
  
   // creating a down casted pointer to playState for use in the exitPrompt state
   downCastedPlayState =  static_cast<StatePlay*>(playState);

   currentState = splashScreenState; // setting the initial startup state to splashScreenState
}

// a function that returns an instance of Game class, it creates one if there isn't one already. but stops creation of anymore than one 
Game * Game::getInstance()
{
	if(!instance)
	{
		 instance = new Game();
	}

	return instance;
}

// returns the downcasted pointer to statePlay so other states can use various statePlay functions like save
StatePlay * Game::getCastedStatePlay(void)
{
	return downCastedPlayState;
}

// function that switches the current state to the new passed in state
void Game::setState(GameState* newState)
{
	if(newState != currentState) // if the new state is not the same as the current state change state
	{
		currentState->exit(); // perform exit actions on new states
		currentState = newState; // change the current State to the New state
		currentState->enter(); // perform enter actions on new states
	}
}

// We should be able to detect when errors occur with SDL if there are 
// unrecoverable errors, then we need to print an error message and quit the program
void Game::exitFatalError(char *message)
{
    std::cout << message << " " << SDL_GetError();
    SDL_Quit();
    exit(1);
}

// Set up rendering context
// Sets values for, and creates an OpenGL context for use with SDL
SDL_Window * Game::setupRC(SDL_GLContext &context)
{

	SDL_Window *window;
    if (SDL_Init(SDL_INIT_VIDEO) < 0) // Initialize video
        exitFatalError("Unable to initialize SDL"); 

    // Request an OpenGL 2.1 context.
	// If you request a context not supported by your drivers, no OpenGL context will be created
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // 8 bit alpha buffering

	// Optional: Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4);
 
    // Create 800x600 window
    window = SDL_CreateWindow("SDL OpenGL Demo for GED",
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        800, 600, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	if (!window) // Check window was created OK
        exitFatalError("Unable to create window");
 
    context = SDL_GL_CreateContext(window); // Create opengl context and attach to window
    SDL_GL_SetSwapInterval(1); // set swap buffers to sync with monitor's vertical refresh rate

	// set up TrueType / SDL_ttf
	if (TTF_Init()== -1)
		exitFatalError("TTF failed to initialise.");
	
	textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);
	if (textFont == NULL)
		exitFatalError("Failed to open font.");
	
	return window;
}

// function that holds the run time game loop 
void Game::run()
{
	SDL_Event sdlEvent; // variable to detect SDL events
	std::cout << "Progress: About to enter main loop" << std::endl;
				
		while (running)	// the event loop
		{
			while (SDL_PollEvent(&sdlEvent))
			{
				if (sdlEvent.type == SDL_QUIT)
					running = false;
				else
					currentState->handleSDLEvent(sdlEvent); 
			}
			currentState->update(); // call the current states update function
		}
}

Game::~Game()
{
	// deleting all of the instatiated State objects
	delete mainMenuState;
	delete playState;
	delete creditsState;
	delete splashScreenState;
	delete characterSelectState;
	delete gameOverState;
	delete gameWonState;
	delete combatState;

	SDL_DestroyWindow(window); // destroying the current window
    SDL_Quit(); // quiting from SDL
}
