#include "stateGameWon.h" // including the stateGameOver header so I can code implementations of the functions
#include "game.h"         // including game so I can call various Game functions (like getSDLWindow, or state related functions)

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateGameWon::StateGameWon()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateGameWon::init()
{
	// creating Labels
	SDL_Color colour = { 255, 255, 0 }; // creating an SDL Color 
	gameWonLabel = new Label("Congratulations, You Win!", TTF_OpenFont("MavenPro-Regular.ttf", 46),  colour);
	gameWonMessageLabel = new Label("You have defeated all of the enemies!", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
	pressKeyToContinueLabel = new Label("Press return key to go back to the Main Menu", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateGameWon::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	// drawing labels to screen
	gameWonLabel->draw(-0.55, 0);
	gameWonMessageLabel->draw(-0.42, -0.1);
	pressKeyToContinueLabel->draw(-0.45, -0.2);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateGameWon::update()
{
	draw();
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateGameWon::handleSDLEvent(SDL_Event const &sdlEvent)
{
	if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{

			// if return is pressed set the state to the main menu
			case SDLK_RETURN:
				Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
				break;
			
			// if any other key is pressed don't do anything
			default:
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateGameWon::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateGameWon::enter()
{
}

// stateGameOver destructor deleting various objects that get instantiated within this class
StateGameWon::~StateGameWon()
{
	//deleting all instantiated Objects
	delete gameWonLabel;
	delete gameWonMessageLabel;
	delete pressKeyToContinueLabel;
}
