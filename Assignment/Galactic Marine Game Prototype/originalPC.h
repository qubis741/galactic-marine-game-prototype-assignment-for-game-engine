#ifndef ORIGINALPC_H
#define ORIGINALPC_H
#include "abstractPC.h"  // including abstractPC so that OriginalPC can inherit from it and gain libraries from it

// as you can see below OriginalPC is a culmination of all of the inherited Abstract classes functions
class OriginalPC: public AbstractPC{

public: 
	OriginalPC();                                                                              // OriginalPC constructor
	int getStrength(){return strength;}                                                        // inline function that returns strength as an int
	int getMaxHealth(){return maxHealth;}                                                      // inline function that returns maxHealth as an int
	int getSpeed(){return speed;}                                                              // inline function that returns speed as an int
	void changeBaseStats(int str, int sp, int hp);                                             // function that is used to change the Base stats of the player
	int getCurrentHealth(){return currentHealth;}                                              // inline function that returns the currentHealth as an int
	void changeCurrentHealth(int hp){currentHealth += hp;}                                     // inline function to change the players current health by passing in the parameter
	int getCurrentMoney(){return currentMoney;}                                                // inline function that returns the currentMoney as an int
	void changeCurrentMoney(int m){currentMoney += m;}                                         // inline function to change the currentMoney by passing in the parameter
	int getNumberOfEnemiesKilled(){return numberOfEnemiesKilled;}                              // inline function that returns the numberOfEnemiesKilled as an int
	void changeNumberOfEnemiesKilled(int amountKilled){numberOfEnemiesKilled += amountKilled;} // inline function that changes the numberOfEnemiesKilled by passing in the parameter 
	void draw();                                                                               // function for calling the relevant draw functions to draw the object on screen
	void drawLabel(float x, float y);                                                          // function for taking in two float variables and then drawing the label at that position 
	void updatePosition(float xPos, float yPos);                                               // function for updating the objects position (the rectangle and the label in this case)
	bool getStateOfVisibility(){return visible;}                                               // inline function that returns the bool visible
	void changeBoolVisible(bool change){visible = change;}                                     // inline function that changes the visible bool by passing in the change parameter
	void resetVariables();                                                                     // function that is used to reset all of the players variables back to there original values
	void setStats(int str, int sp, int chp, int mhp, int cm, int ek);                          // function to set the stats, difference from changeStat function is that this one is for setting entirely new stats by using the =, rather than editing pre-existing stats as well as setting currentMoney and enemies killed
	~OriginalPC();                                                                             // OriginalPC destructor

private:
	// variables and/or object pointers that OriginalPC requires the identifiers are descriptive
	int strength; 
	int speed;
	int currentHealth;
	int maxHealth;
	int currentMoney;
	int numberOfEnemiesKilled;
	bool visible;
};

#endif