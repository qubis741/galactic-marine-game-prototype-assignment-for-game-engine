#ifndef HEALTHPACKITEM_H
#define HEALTHPACKITEM_H
#include "abstractItem.h" // including abstractItem so that HealthPackItem can inherit from it and gain libraries from it

// as you can see below HealthPackItem is a culmination of all of the inherited Abstract classes functions
class HealthPackItem: public AbstractItem{

public:
	HealthPackItem();                                      // HealthPackItem constructor
    void drawLabel(float x, float y);                      // function for taking in two float variables and then drawing the label at that position 
	void draw();                                           // function for calling the relevant draw functions to draw the object on screen
	void updatePosition(float xPos, float yPos);           // function for updating the objects position (just the rectangle in this case)
	int getStatIncrease(){return healthIncrease;}	       // inline function that returns the HealthPackItem's health increase
	void resetVariables();                                 // function for reseting the variables of HealthPackItem (mostly the visible boolean but in future there could be more) 
	bool getStateOfVisibility(){return visible;}            // inline function that returns the state of the visible boolean
	void changeBoolVisible(bool change){visible = change;} // function for changing the visible boolean
	~HealthPackItem();                                     // HealthPackItem destructor

private:
	// variables and/or object pointers that HealthPackItem requires the identifiers are descriptive
	int healthIncrease;
	bool visible;
};

#endif
