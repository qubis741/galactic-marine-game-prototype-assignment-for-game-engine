#include "fodderNPC.h" // including FodderNPC header file so I can create implementations for some of its functions

// assigning values and creating objects within the FodderNPC constructor
FodderNPC::FodderNPC(): maxHealth(3), strength(3), speed(7), moneyDrop(1), dropRate(5), visible(true)
{	
	// passing in the Color parameters as well as setting the size while Creating this New Rect Object
	marker = new Rect(0.0f, 1.0f, 0.0f);

	//set the color, text and size of the label
	SDL_Color colour = { 255, 255, 0 };
	name = new Label("Fodder",TTF_OpenFont("MavenPro-Regular.ttf", 24),colour);
}

// used to update/change the position of the Rect object called marker by passing in parameters for x and y
void FodderNPC::updatePosition(float xPos, float yPos)
{
	//setting the marker position
	marker->setX(xPos);
	marker->setY(yPos);
}

//used to change the FodderNPCs base stats(strength, speed, health) by passing in values  
void FodderNPC::changeBaseStats(int str, int sp, int hp)
{
	strength += str;
	speed += sp;
	maxHealth += hp;
}

// used to reset the FodderNPCs variables to there default state
void FodderNPC::resetVariables()
{
	visible = true;
	maxHealth = 3;
	strength = 3;
	speed = 7;
	moneyDrop = 1;
	dropRate = 5;
}

// used to draw the parts of FodderNPC to screen that are required(could for instance also draw the label to screen at the marker position)
void FodderNPC::draw()
{	
	// calling the marker(Rect) draw function
	marker->draw();
}

// for drawing the FodderNPC's name(label) to screen pass in parameters for where you wish it to be drawn
void FodderNPC::drawLabel(float x, float y)
{
	// calling the name(Label) draw function 
	name->draw(x, y);
}

// FodderNPC deconstructor delete objects and do other things that are required for the Deletion of FodderNPC
FodderNPC::~FodderNPC()
{
	// delete the objects instantiated
	delete name;
	delete marker;
}