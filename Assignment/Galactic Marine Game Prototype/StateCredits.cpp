#include "StateCredits.h" // including the stateCredits header so I can code implementations of the functions
#include "game.h"         // including game so I can call various Game functions (like getSDLWindow, or state related functions)
#include <ctime>          // C time libraries for rand and time functions

// constructor that calls init(where everyhting that needs to be instantiated or set will get set) 
StateCredits::StateCredits()
{
	init();
}

// function to initilize the various objects and variables within this state
void StateCredits::init()
{
	// creating Labels
	SDL_Color colour = { 255, 255, 0 };
	creditsTitleLabel = new Label("Credits", TTF_OpenFont("MavenPro-Regular.ttf", 46),  colour);
	programmerLabel = new Label("Prototype Programmer: Andrew Gozillon", TTF_OpenFont("MavenPro-Regular.ttf", 24),  colour);
	conceptLabel = new Label( "Prototype Concept: Daniel Livingstone", TTF_OpenFont("MavenPro-Regular.ttf", 24), colour);
}

// function to draw all of the various things that are required to be drawn during this state
void StateCredits::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear window
  
	//drawing labels to screen
	creditsTitleLabel->draw(-0.15, 0.9);
	programmerLabel->draw(-0.3, 0.7);
	conceptLabel->draw(-0.3, 0.5);

	SDL_GL_SwapWindow(Game::getInstance()->getSDLWindow()); // swap buffers
}

// function that get continually called to update this state, functions like draw should go in here so that if any changes
// happen to the objects being drawn then the draw function will instantly get called and the screen will be updated
void StateCredits::update()
{ 
	draw(); // calling draw so that it updates the screen
	
	// creating temporary clock_t variables to hold the clock ticks and to set the exit time of this state
	clock_t updatingTime;
	clock_t timeInCredits;
	clock_t exitTimeOfCredits = 30;
	
	// constantly updating the time within the state
	updatingTime = clock() / CLOCKS_PER_SEC; 
	
	// taking away the entry time from the updated time to calculate time spent in this state
	timeInCredits = updatingTime - entryTime; 

	// if the time spent in the credits is greater than exitTimeOfCredits set the state back to main menu
	if(timeInCredits > exitTimeOfCredits) 
	{
		Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
	}	
}

// function for handling various keyboard input and changing/effecting the current state depending on choice
void StateCredits::handleSDLEvent(SDL_Event const &sdlEvent)
{
  if (sdlEvent.type == SDL_KEYDOWN)
	{
		switch( sdlEvent.key.keysym.sym )
		{

			// if any key is pressed set the state back to the main menu
			default:
				Game::getInstance()->setState(Game::getInstance()->getMainMenuState());
				break;

		}
	}
}

// function that is called apon exit of this state, functions that you wish to be called or variables that you wish to be
// changed when this state changes to another state should go within here
void StateCredits::exit()
{
}

// function that is called apon entering this state, functions that you wish to be called or variables that you wish to be 
// changed when switching to this state should go within here
void StateCredits::enter()
{
	// taking the time of entry to the state in seconds
	entryTime = clock() / CLOCKS_PER_SEC; 
}

// stateCredits destructor deleting various objects that get instantiated within this class
StateCredits::~StateCredits()
{
	// deleting any objects I instantiated within this class
	delete programmerLabel;
	delete conceptLabel;
	delete creditsTitleLabel;
}